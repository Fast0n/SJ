# SJ: Social Jokes


```
  _____      _       _____            _       _        _       _             
 / ____|    | | _   / ____|          (_)     | |      | |     | |           
| (___      | |(_) | (___   ___   ___ _  __ _| |      | | ___ | | _____  ___ 
 \___ \ _   | |     \___ \ / _ \ / __| |/ _` | |  _   | |/ _ \| |/ / _ \/ __| 
 ____) | |__| | _   ____) | (_) | (__| | (_| | | | |__| | (_) |   <  __/\__ \ 
|_____/ \____/ (_) |_____/ \___/ \___|_|\__,_|_|  \____/ \___/|_|\_\___||___/
```


<p align="center">
  <img src="https://thumbs.gfycat.com/LonelySlowCoyote-size_restricted.gif?raw=true" alt="SJ: Social Jokes"/>
</p>


# Dona per il progetto
Per la creazione di questo progetto è stato investito del tempo, se ritieni che sia utile dona allo sviluppatore.


[![gitcheese.com](https://s3.amazonaws.com/gitcheese-ui-master/images/badge.svg)](https://www.gitcheese.com/donate/users/5260133/repos/95372423)
