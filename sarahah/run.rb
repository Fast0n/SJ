puts "  _____      _       _____            _       _        _       _             "
puts " / ____|    | | _   / ____|          (_)     | |      | |     | |            "
puts "| (___      | |(_) | (___   ___   ___ _  __ _| |      | | ___ | | _____  ___ "
puts " \\___ \\ _   | |     \\___ \\ / _ \\ / __| |/ _` | |  _   | |/ _ \\| |/ / _ \\/ __| "
puts " ____) | |__| | _   ____) | (_) | (__| | (_| | | | |__| | (_) |   <  __/\\__ \\ "
puts "|_____/ \\____/ (_) |_____/ \\___/ \\___|_|\\__,_|_|  \\____/ \\___/|_|\\_\\___||___/"
puts ""
#libreria
require 'watir'

#crea un file con le domande
@browser = Watir::Browser.new :chrome
@domande = File.new("domande_" + Time.now.strftime("%H%M%S") + ".txt", "w")

puts "Apro sarahah"
def main ()
  #apre il profilo e aspetta 2sec
  @browser.goto "https://dev.sarahah.com/"
  sleep 2
  
  #controlla la possibilità di fare domande anonime
  if @browser.span(:class, "icon icon-check").exists?
    puts "Questo utente non consente agli utenti anonimi di inviare messaggi."
    exit 0
  end

  # i = genera un numero random da 0 - 8
  i = rand(1...8)
  case i

  when 1
    #crea una serie di caratteri a caso con lettere e numeri
    random_string = ('0'..'z').to_a.shuffle.first(400).join
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "Text").set random_string
    @domande.puts(random_string)
  when 2
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "Text").set 'domanda 2'
    @domande.puts('domanda 2')
  when 3
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "Text").set 'domanda 3'
    @domande.puts('domanda 3')
  when 4
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "Text").set 'domanda 4'
    @domande.puts('domanda 4')
  when 5
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "Text").set 'domanda 5'
    @domande.puts('domanda 5')
  when 6
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "Text").set 'domanda 6'
    @domande.puts('domanda 6')
  when 7
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "Text").set 'domanda 7'
    @domande.puts('domanda 7')
  when 8
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "Text").set 'domanda 8'
    @domande.puts('domanda 8')
  end
  #aspetta 2sec
  sleep 2
  #scrive sul file l'ora e i minuti di quando si clicca il tasto 'send'
  puts "click alle " + Time.now.strftime("%H:%M")
  @domande.puts("click alle " + Time.now.strftime("%H:%M"))
  #click del tasto 'send'
  @browser.button(:id => 'Send').click
  #aspetta 120sec non toccare, o si attiverà un bot detection
  sleep 120
  #torna a 'main' per un loop infinito
  return main
end
main
