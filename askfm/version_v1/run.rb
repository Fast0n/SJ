puts "  _____      _       _____            _       _        _       _             "
puts " / ____|    | | _   / ____|          (_)     | |      | |     | |            "
puts "| (___      | |(_) | (___   ___   ___ _  __ _| |      | | ___ | | _____  ___ "
puts " \\___ \\ _   | |     \\___ \\ / _ \\ / __| |/ _` | |  _   | |/ _ \\| |/ / _ \\/ __| "
puts " ____) | |__| | _   ____) | (_) | (__| | (_| | | | |__| | (_) |   <  __/\\__ \\ "
puts "|_____/ \\____/ (_) |_____/ \\___/ \\___|_|\\__,_|_|  \\____/ \\___/|_|\\_\\___||___/"
puts ""
#libreria
require 'watir'
require_relative 'credentials'

username = $username
password = $password

#crea un file con le domande
@browser = Watir::Browser.new :chrome
@domande = File.new("domande_" + Time.now.strftime("%H%M%S") + ".txt", "w")
puts "Apro ask"
#ridimensiona la finestra di chrome
@browser.window.resize_to(1000, 800)
@browser.goto "https://ask.fm/login"

#dati per il login *obbligatorio e tasto invio
@browser.text_field(:name => "login").set "#{username}"
@browser.text_field(:name => "password").set "#{password}"
@browser.send_keys :enter
#aspetta 5sec
sleep 5
puts "Apro il profilo"
def main ()

  #apre il profilo e aspetta 2sec
  @browser.goto "https://ask.fm/dev"
  sleep 2

  #controlla la possibilità di fare domande anonime
  if @browser.input(:disabled, "disabled").exists?
    puts "Non puoi fare domande in anonimo"
    exit 0
  end

  # i = genera un numero random da 0 - 8
  i = rand(1...8)
  case i

  when 1
    #crea una serie di caratteri a caso con lettere e numeri
    random_string = ('0'..'z').to_a.shuffle.first(400).join
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "question_question_text").set random_string
    @domande.puts(random_string)
  when 2
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "question_question_text").set 'domanda 2'
    @domande.puts('domanda 2')
  when 3
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "question_question_text").set 'domanda 3'
    @domande.puts('domanda 3')
  when 4
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "question_question_text").set 'domanda 4'
    @domande.puts('domanda 4')
  when 5
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "question_question_text").set 'domanda 5'
    @domande.puts('domanda 5')
  when 6
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "question_question_text").set 'domanda 6'
    @domande.puts('domanda 6')
  when 7
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "question_question_text").set 'domanda 7'
    @domande.puts('domanda 7')
  when 8
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "question_question_text").set 'domanda 8'
    @domande.puts('domanda 8')
  end
  #aspetta 2sec
  sleep 2
  #scrive sul file l'ora e i minuti di quando si clicca il tasto 'send'
  puts "click alle " + Time.now.strftime("%H:%M")
  @domande.puts("click alle " + Time.now.strftime("%H:%M"))
  #click del tasto 'send'
  @browser.button(:class => 'btn-primary').click
  #aspetta 120sec non toccare, o si attiverà un bot detection
  sleep 180
  #torna a 'main' per un loop infinito
  return main
end
main
