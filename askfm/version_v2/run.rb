#!/usr/bin/env ruby
puts "  _____      _       _____            _       _        _       _             "
puts " / ____|    | | _   / ____|          (_)     | |      | |     | |            "
puts "| (___      | |(_) | (___   ___   ___ _  __ _| |      | | ___ | | _____  ___ "
puts " \\___ \\ _   | |     \\___ \\ / _ \\ / __| |/ _` | |  _   | |/ _ \\| |/ / _ \\/ __| "
puts " ____) | |__| | _   ____) | (_) | (__| | (_| | | | |__| | (_) |   <  __/\\__ \\ "
puts "|_____/ \\____/ (_) |_____/ \\___/ \\___|_|\\__,_|_|  \\____/ \\___/|_|\\_\\___||___/"
puts ""
require 'watir'
require_relative './functions.rb'
require_relative './questions.rb'

@o_mail = "account/mail.txt"
@o_password = "account/password.txt"
@o_username = "account/username.txt"
@o_domande = Dir['account/domande_*.txt']
@o_profile = "account/profile.txt"

create_file
linee_file = File.foreach(@o_mail).count

if !File.exist?(@o_profile)
  puts "Nome utente del profilo a cui fare le domande: "
  profile = gets.chomp
  File.new(@o_profile, "w")
  open(@o_profile, 'a') { |f| f.puts profile}
end

puts "Quante domande vuoi fare? (Max 10)"
num = gets.chomp

while true
  if num.to_i.to_s == num && num.to_i <= 10
    if linee_file <  num.to_i
      @numero = num.to_i - linee_file
      puts "Email mancanti " + @numero.to_s + " provvedo a crearle"

      $i = 0
      begin
        create_mail
        $i +=1
      end while $i < @numero
      create_account
    else
      puts "File trovati \nUsername:\n" + File.read(@o_username).to_s + "\nPassword:\n" + File.read(@o_password).to_s
      login
    end
    break
  else
    print "Quante domande vuoi fare? (Max 10)\n"
    num = gets.chomp
  end
end
