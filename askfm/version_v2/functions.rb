def create_file
  create_profile
  if !File.exist?(@o_mail) || !File.exist?(@o_password) || !File.exist?(@o_username)
    puts "Genero i file"
    File.new(@o_mail, "w")
    @password = File.new(@o_password, "w")
    @username = File.new(@o_username, "w")
    @domande = File.new("account/domande_" + Time.now.strftime("%H%M%S") + ".txt", "w")
  end
end

def create_mail
  @browser = Watir::Browser.new :chrome
  @browser.goto "https://10minutemail.net/"
  register_mail = @browser.input(:id => 'fe_text').value
  register_username = Array[*('a'..'z')].sample(8).join
  register_password = Array[*('a'..'z')].sample(8).join
  open(@o_mail, 'a') { |f| f.puts register_mail}
  open(@o_username, 'a') { |f| f.puts register_username}
  open(@o_password, 'a') { |f| f.puts register_password}
  sleep 2
  @browser.close
  sleep 1
end

def create_account
  $i = 0
  begin
    @browser = Watir::Browser.new :chrome
    @browser.goto "https://ask.fm/signup"
    @browser.text_field(:id => "user_email").set IO.readlines(@o_mail)[$i]
    @browser.text_field(:id => "user_name").set 'Mario Rossi'
    @browser.text_field(:id => "user_login").set IO.readlines(@o_username)[$i]
    @browser.text_field(:id => "user_password").set IO.readlines(@o_password)[$i]
    @browser.select_list(:id => "date_day").select_value("1")
    @browser.select_list(:id => "date_month").select_value("1")
    @browser.select_list(:id => "date_year").select_value("1990")
    @browser.select_list(:id => "user_gender_id").select_value("1")
    @browser.select_list(:id => "user_language_code").select_value("it")
    sleep 2
    @browser.send_keys :enter

    if @browser.url == "https://ask.fm/signup"
      puts "C'è qualche problema con la registrazione"
      exit 0
    end

    create_questions
    $i +=1
  end while $i < @numero
end

def create_profile
  if !File.exist?(@o_profile)
    Dir.mkdir 'account'
    puts "Nome utente del profilo a cui fare le domande: "
    profile = gets.chomp
    File.new(@o_profile, "w")
    open(@o_profile, 'a') { |f| f.puts profile}
  end
end

def login
  @i = 0
  until @i <= @numero.to_i  do
    @browser = Watir::Browser.new :chrome
    @browser.window.resize_to(1000, 800)
    @browser.goto "https://ask.fm/login"
    @browser.text_field(:name => "login").set IO.readlines(@o_username)[@i]
    @browser.text_field(:name => "password").set IO.readlines(@o_password)[@i]
    sleep 2
    @browser.send_keys :enter

    if @browser.url == "https://ask.fm/login"
      puts "C'è qualche problema con il login"
      exit 0
    end

    create_questions
    logout
    @i +=1
  end
end

def logout
  @browser.window.resize_to(400, 800)
  @browser.a(:class => "btn-action-icon icon-hamburger").click
  sleep 2
  @browser.a(:class => "icon-logout").click
  sleep 2
  @browser.close
end
