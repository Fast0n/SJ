def create_questions
  #apre il profilo e aspetta 2sec
  @browser.window.resize_to(1000, 800)
  @browser.goto 'https://ask.fm/' + IO.readlines(@o_profile)[0]
  sleep 2
  #controlla la possibilità di fare domande anonime
  if @browser.input(:disabled, "disabled").exists?
    puts "Non puoi fare domande in anonimo"
    exit 0
  end

  # i = genera un numero random da 0 - 8
  i = rand(1..8)
  case i

  when 1
    #crea una serie di caratteri a caso con lettere e numeri
    random_string = ('0'..'z').to_a.shuffle.first(400).join
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "question_question_text").set random_string
    #@o_domande.puts(random_string)
  when 2
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "question_question_text").set 'domanda 2'
    #@o_domande.puts('domanda 2')
  when 3
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "question_question_text").set 'domanda 3'
    #@o_domande.puts('domanda 3')
  when 4
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "question_question_text").set 'domanda 4'
    #@o_domande.puts('domanda 4')
  when 5
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "question_question_text").set 'domanda 5'
    #@o_domande.puts('domanda 5')
  when 6
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "question_question_text").set 'domanda 6'
    #@o_domande.puts('domanda 6')
  when 7
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "question_question_text").set 'domanda 7'
    #@o_domande.puts('domanda 7')
  when 8
    #scrive la domanda sul social e sul file
    @browser.textarea(:id => "question_question_text").set 'domanda 8'
    #@o_domande.puts('domanda 8')
  end
  #aspetta 3sec
  sleep 3
  #scrive sul file l'ora e i minuti di quando si clicca il tasto 'send'
  puts "click alle " + Time.now.strftime("%H:%M")
  #@o_domande.puts("click alle " + Time.now.strftime("%H:%M"))
  #click del tasto 'send'
  @browser.button(:class => 'btn-primary').click
  sleep 5
end
